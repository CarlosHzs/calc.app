package calc.app.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.poi.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


public class FileLister {

	public void listFiles() {

		try (Stream<Path> walk = Files.walk(Paths.get("F:\\datos\\"))) {

			List<String> result = walk.map(x -> x.toString()).filter(f -> f.endsWith(".xlsx"))
					.collect(Collectors.toList());

			result.forEach(r -> sheetOpener(r));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void sheetOpener(String r) {

		try {
			Workbook wb = WorkbookFactory.create(new File(r));
		    for (Sheet sheet : wb ) {
		        for (Row row : sheet) {
		            for (Cell cell : row) {
		            	System.out.println(cell.toString());
		            }
		        }
		    }
		} catch (EncryptedDocumentException | IOException e) {
			e.printStackTrace();
		}
		
		

	}

}